use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use serde_json::value::Value::Array;
use std::path::Path;
use std::thread;

use super::encoding::*;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Replay {
    pub file: String,
    pub date: NaiveDate,
    pub time: NaiveTime,
    pub stage: String,
    pub characters: String,
    pub start_frame: isize,
    pub end_frame: isize,
}

impl Replay {
    fn new(
        file: String,
        date: NaiveDate,
        time: NaiveTime,
        stage: String,
        characters: String,
        start_frame: isize,
        end_frame: isize,
    ) -> Replay {
        Replay {
            file,
            date,
            time,
            stage,
            characters,
            start_frame,
            end_frame,
        }
    }
}

pub fn get_replays(folder: &Path) -> Vec<Replay> {
    let mut threads = Vec::with_capacity(1000);
    folder
        .read_dir()
        .expect("Invalid directory!")
        .for_each(|entry| {
            if let Ok(entry) = entry {
                threads.push(thread::spawn(move || -> Option<Replay> {
                    let path = &entry.path();
                    let game = peppi::game(path);
                    if let Ok(game) = game {
                        let json = serde_json::to_value(&game).unwrap();
                        let file = path.file_name().unwrap();
                        let datetime = json["metadata"]["startAt"]
                            .to_string()
                            .trim_matches('"')
                            .parse::<DateTime<Utc>>()
                            .expect("Couldn't read date/time.")
                            .naive_utc();
                        let end_frame = json["metadata"]["lastFrame"]
                            .to_string()
                            .parse::<isize>()
                            .unwrap();
                        let stage_id = json["start"]["stage"].to_string().parse::<usize>().unwrap();
                        let stage = match_stage(stage_id);
                        let players = &json["start"]["players"];
                        let mut characters = Vec::with_capacity(4);
                        if let Array(players) = players {
                            characters = players
                                .into_iter()
                                .filter(|value| value.to_string() != "null")
                                .map(|character| {
                                    match_character(
                                        character["character"]
                                            .to_string()
                                            .parse::<usize>()
                                            .unwrap(),
                                    )
                                })
                                .collect();
                        }
                        let replay = Replay::new(
                            file.to_str().unwrap().to_string(),
                            datetime.date(),
                            datetime.time(),
                            stage,
                            format!("{} vs. {}", characters[0], characters[1]).to_string(),
                            -123,
                            end_frame,
                        );
                        Some(replay)
                    } else {
                        None
                    }
                }));
            }
        });
    threads
        .into_iter()
        .filter_map(|thread| thread.join().unwrap())
        .collect()
}
