// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue/dist/vue.js'
import App from './App'
import Buefy from 'buefy'
import Router from 'vue-router'
import Home from '@/components/home/Home'
import Replays from '@/components/replays/Replays'
import Generate from '@/components/generate/Generate'
import Queue from '@/components/queue/Queue'
import Settings from '@/components/settings/Settings'
import '@fortawesome/fontawesome-free/css/all.css'
import 'buefy/dist/buefy.css'

// global elements
require('@/components/index.js');

Vue.use(Buefy)

Vue.config.productionTip = false

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/replays',
      component: Replays
    },
    {
      path: '/generate',
      component: Generate
    },
    {
      path: '/queue',
      component: Queue
    },
    {
      path: '/settings',
      component: Settings
    }
  ]
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
