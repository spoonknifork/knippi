# Project Knippi

The goals of this project is to combine the elegant approach to replays and
automatically sourcing combos that `Project Slippi Desktop App` and
`Project Clippi` have. `Project Knippi` is gonna be written in Rust using
Tauri for the user-interface, should be small in size and perform fast.

# Current State

![](./currentstate.png)

# Roadmap

- Replays
- Generate combos
- Showcase combo when highlighting replay?
- Other?

# Technology Stack

- Rust
- Tauri
- Vue
    - Buefy
    - Bulma
    - Font Awesome

# Building

For a detailed explanation on getting dependencies and such, use this
[guide](https://github.com/tauri-apps/vue-cli-plugin-tauri).


``` bash
# install dependencies
yarn

# open dev window
yarn tauri:serve

# build for production and view the bundle analyzer report
yarn tauri:build
```
